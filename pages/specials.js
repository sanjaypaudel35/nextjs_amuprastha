import React from "react";
import MetaHead from "components/MetaHead";
import PageLayout from "components/PageLayout";
import SpecialMenu from "components/Landing/SpecialMenu/SpecialMenu";

const metaContent = {
  title: "Special Menu",
  description: "Special menus available at amuprastha",
  keyword: "amuprastha",
};

const SpecialsPage = () => {
  return (
    <React.Fragment>
      <MetaHead meta={metaContent} />
      <PageLayout title="Chefs" contentFullWidth={true}>
        <SpecialMenu />
      </PageLayout>
    </React.Fragment>
  );
};
export default SpecialsPage;
