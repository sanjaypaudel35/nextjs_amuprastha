import PageLayout from "components/PageLayout";
import Link from "next/link";

const TestPage = (props) => {
  return (
    <>
      <PageLayout title="Test Page" contentFullWidth={false}>
        <h1>This is test page</h1>
        <span>
          <Link href="/menus" className="test-class">
            <a className="testClass">click this link</a>
          </Link>
        </span>
      </PageLayout>
    </>
  );
};

export default TestPage;
