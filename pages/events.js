import React from "react";
import MetaHead from "components/MetaHead";
import PageLayout from "components/PageLayout";
import Events from "components/Landing/Events/Events";

const metaContent = {
  title: "Events",
  description: "Organzie and manage events at amuprastha",
  keyword: "amuprastha",
};

const EventsPage = () => {
  return (
    <React.Fragment>
      <MetaHead meta={metaContent} />

      {/* title props is title showing on breadcrumbs head, 
    contentFullWidth is toggle between classname container or container-fluid */}
      <PageLayout title="Events" contentFullWidth={true}>
        <Events />
      </PageLayout>
    </React.Fragment>
  );
};
export default EventsPage;
