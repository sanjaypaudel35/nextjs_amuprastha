import "../styles/globals.css";
import LanguageProvider from "store/LanguageProvider";

function MyApp({ Component, pageProps }) {
  return (
    <LanguageProvider>
      <Component {...pageProps} />
    </LanguageProvider>
  );
}

export default MyApp;
