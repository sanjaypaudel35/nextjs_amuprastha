import React, { Suspense, useEffect } from "react";
import { useRef } from "react";
import SiteConfig from "config/SiteConfig";
import WhyUs from "components/Landing/WhyUs";
import About from "components/Landing/About";
import TopBar from "components/Landing/TopBar";
import Footer from "components/Landing/Footer/Footer";
import Contact from "components/Landing/Contact";
// import Menus from "components/Landing/Menu/Menus";
import Chefs from "components/Landing/Chefs/Chefs";
import BookTable from "components/Landing/BookTable";
import Header from "components/Landing/Header/Header";
import Events from "components/Landing/Events/Events";
import PhotoGallery from "components/Landing/Gallery/PhotoGallery";
import HeroCarousel from "components/Landing/Carousel/HeroCarousel";
// import SpecialMenu from "components/Landing/SpecialMenu/SpecialMenu";
import Testimonial from "components/Landing/Testimonial/Testimonial";
import FallBackScreen from "components/FallBackScreen";

import { ThemeProvider } from "styled-components";

import MetaHead from "components/MetaHead";
import Menus from "components/Landing/Menu/Menus";
import SpecialMenu from "components/Landing/SpecialMenu/SpecialMenu";

// const About = React.lazy(() => import("components/Landing/About"));
// const WhyUs = React.lazy(() => import("components/Landing/WhyUs"));
// const Menus = React.lazy(() => import("components/Landing/Menu/Menus"));

// const SpecialMenu = React.lazy(() =>
//   import("components/Landing/SpecialMenu/SpecialMenu")
// );

const metaContent = {
  title: "Amuprastha",
  description: "amuprastha",
  keyword: "amuprastha",
};

const Landing = () => {
  const headerRef = useRef();

  const handleScroll = () => {
    let headerReference = document.getElementById("header");
    let headerOffsetTop = headerReference.offsetTop;

    if (window.pageYOffset > headerOffsetTop) {
      headerReference.style.background = "rgba(26, 24, 22, 0.85)";
      headerReference.style.top = "0px";
    } else {
      headerReference.style.background = "";
      headerReference.style.top = "50px";
    }
  };
  useEffect(() => {
    if (typeof window !== "undefined") {
      // Perform localStorage action
      window.addEventListener("scroll", handleScroll);
    }
  }, []);

  return (
    <React.Fragment>
      <MetaHead meta={metaContent} />
      <ThemeProvider theme={SiteConfig.Theme}>
        <TopBar />
        <Header ref={headerRef} />
        <HeroCarousel />
        <main id="main">
          <About />
          <WhyUs />
          <Menus />
          <SpecialMenu />

          <Events />
          <PhotoGallery />
          <Chefs />
          <Testimonial />
          <Contact />
          <Footer />
        </main>
      </ThemeProvider>
    </React.Fragment>
  );
};
export default Landing;
