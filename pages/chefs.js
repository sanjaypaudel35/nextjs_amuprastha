import PageLayout from "components/PageLayout";

import React from "react";
import MetaHead from "components/MetaHead";
import Chefs from "components/Landing/Chefs/Chefs";

const metaContent = {
  title: "Chefs",
  description: "professional chefs of amuprastha",
  keyword: "amuprastha",
};

const ChefsPage = () => {
  return (
    <React.Fragment>
      <MetaHead meta={metaContent} />
      <PageLayout title="Chefs" contentFullWidth={true}>
        <Chefs />
      </PageLayout>
    </React.Fragment>
  );
};
export default ChefsPage;
