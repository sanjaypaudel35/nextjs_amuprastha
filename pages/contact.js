import React from "react";
import PageLayout from "components/PageLayout";
import Contact from "components/Landing/Contact";
import MetaHead from "components/MetaHead";

const metaContent = {
  title: "Contact",
  description: "contact detail of amuprastha Boutique",
  keyword: "amuprastha",
};

const ContactPage = () => {
  return (
    <React.Fragment>
      <MetaHead meta={metaContent} />
      <PageLayout title="Contact" contentFullWidth={false}>
        <Contact />
      </PageLayout>
    </React.Fragment>
  );
};
export default ContactPage;
