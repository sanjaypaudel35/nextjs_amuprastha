import PageLayout from "components/PageLayout";
import React from "react";
import PhotoGallery from "components/Landing/Gallery/PhotoGallery";
import MetaHead from "components/MetaHead";

const metaContent = {
  title: "Gallery",
  description: "photo gallery of amuprashta",
  keyword: "amuprastha",
};
const GalleryPage = () => {
  return (
    <React.Fragment>
      <MetaHead meta={metaContent} />
      <PageLayout title="Photo Gallery" contentFullWidth={true}>
        <PhotoGallery />
      </PageLayout>
    </React.Fragment>
  );
};
export default GalleryPage;
