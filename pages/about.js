import PageLayout from "components/PageLayout";
import About from "components/Landing/About";
import React from "react";
import MetaHead from "components/MetaHead";

const metaContent = {
  title: "About us",
  description: "amuprastha",
  keyword: "amuprastha",
};

const AboutUs = () => {
  return (
    <React.Fragment>
      <MetaHead meta={metaContent} />
      <PageLayout title="About Us" contentFullWidth={false}>
        <About />
      </PageLayout>
    </React.Fragment>
  );
};
export default AboutUs;
