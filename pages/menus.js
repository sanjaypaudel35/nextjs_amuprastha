import PageLayout from "components/PageLayout";
import React from "react";
import Menus from "components/Landing/Menu/Menus";
import Head from "next/head";
import MetaHead from "components/MetaHead";

const metaContent = {
  title: "Menus",
  description: "All the available menus at amuprastha",
  keyword: "amuprastha",
};

const MenuPage = () => {
  return (
    <React.Fragment>
      <PageLayout title="Menus" contentFullWidth={false}>
        <MetaHead meta={metaContent} />
        <Menus />
      </PageLayout>
    </React.Fragment>
  );
};
export default MenuPage;
