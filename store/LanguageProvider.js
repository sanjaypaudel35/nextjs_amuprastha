import React, { useState } from "react";
import LanguageContext from "./language-context";

const LanguageProvider = (props) => {
  const [stateLang, setStateLang] = useState("en");
  const [stateLangId, setStateLangId] = useState(1);

  const languageChangeHandler = (lang, langId) => {
    setStateLang(lang);
    setStateLangId(langId);
  };
  const langContext = {
    stateLang: stateLang,
    stateLangId: stateLangId,
    changeLang: languageChangeHandler,
  };
  return (
    <LanguageContext.Provider value={langContext}>
      {props.children}
    </LanguageContext.Provider>
  );
};
export default LanguageProvider;
