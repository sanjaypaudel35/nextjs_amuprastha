import React from "react";

const LanguageContext = React.createContext({
  stateLang: "en",
  stateLangId: "1",
});

export default LanguageContext;
