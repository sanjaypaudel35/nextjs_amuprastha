import classes from "./BreadcrumbsSection.module.css";
import Link from "next/link";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";

const BreadCrumbs = (props) => {
  const title = props.title ? props.title : "";
  return (
    <div className={classes.amuBreadcrumbs}>
      <div className="container">
        <div className="d-flex justify-content-between align-items-center">
          <h2>{props.title}</h2>
          <ul className={classes.breadcrumbsMenu}>
            <li>
              <Link href="/">
                <PrimaryColorSpan>Home</PrimaryColorSpan>
              </Link>
            </li>
            &nbsp;&nbsp;/&nbsp;&nbsp;<li>{title}</li>
          </ul>
        </div>
      </div>
    </div>
  );
};
export default BreadCrumbs;
