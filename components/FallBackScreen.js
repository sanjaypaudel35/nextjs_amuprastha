const FallBackScreen = (props) => {
  const styles = {
    display: "flex",
    justifyContent: "center",
    alignItems: "Center",
    height: "200px",
  };

  return (
    <>
      <div style={styles}>{props.children}</div>
    </>
  );
};
export default FallBackScreen;
