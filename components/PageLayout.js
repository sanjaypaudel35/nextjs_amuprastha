import React from "react";

import SiteConfig from "Config/SiteConfig";

import Footer from "components/Landing/Footer/Footer";
import BreadCrumbs from "./BreadCrumbs";

import { ThemeProvider } from "styled-components";
import PageHeader from "./PageHeader";

const PageLayout = (props) => {
  return (
    <React.Fragment>
      <ThemeProvider theme={SiteConfig.Theme}>
        <PageHeader />
        {/* title props is title showing on breadcrumbs head, 
    contentFullWidth is toggle between classname container or container-fluid */}
        <BreadCrumbs title={props.title} />
        <div
          className={props.contentFullWidth ? "container-fuild" : "container"}
        >
          <div className="">{props.children}</div>
        </div>
        <Footer />
      </ThemeProvider>

      {/*Header*/}
    </React.Fragment>
  );
};

export default PageLayout;
