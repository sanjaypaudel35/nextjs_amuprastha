import React from "react";
import TestimonialConfig from "Config/TestimonialConfig";
import TestimonialItem from "./TestimonialItem";

import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, {
  Navigation,
  Pagination,
  Autoplay,
  Virtual,
} from "swiper/core";

// import "./styles.css";
SwiperCore.use([Navigation, Pagination, Autoplay, Virtual]);

const Testimonial = () => {
  const renderTestimonials = TestimonialConfig.Testimonialcontent.map(
    (testimonial) => {
      return <TestimonialItem key={testimonial.id} testimonial={testimonial} />;
    }
  );
  return (
    <section id="testimonials" className="testimonials">
      <div classNameName="container position-relative">
        <div
          className="testimonials-slider swiper"
          data-aos="fade-up"
          data-aos-delay="100"
        >
          <Swiper
            id="swiper"
            virtual
            slidesPerView={1}
            // slidesPerColumn={2}
            // slidesPerColumnFill="row"
            spaceBetween={30}
            // slidesPerGroup={2}
            autoplay
            loop
            //   onReachEnd={() => {
            //     console.log("reach end");
            //     const tmp = slides.unshift();
            //     slides.push(tmp);
            //   }}
            navigation
            pagination
          >
            {/* {renderTestimonials} */}
            <SwiperSlide style={{ listStyle: "none" }}>
              <div className="testimonial-item">
                <img
                  src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200"
                  className="testimonial-img"
                  alt="alt text"
                />
                <h3>this is name</h3>
                <h4>this is position</h4>
                <div className="stars">
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                </div>
                <p>
                  <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                  this is content paragraph
                  <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </SwiperSlide>
            <SwiperSlide style={{ listStyle: "none" }}>
              <div className="testimonial-item">
                <img
                  src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200"
                  className="testimonial-img"
                  alt="alt text"
                />
                <h3>this is name</h3>
                <h4>this is position</h4>
                <div className="stars">
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                </div>
                <p>
                  <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                  this is content paragraph
                  <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </SwiperSlide>
            <SwiperSlide style={{ listStyle: "none" }}>
              <div className="testimonial-item">
                <img
                  src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200"
                  className="testimonial-img"
                  alt="alt text"
                />
                <h3>this is name</h3>
                <h4>this is position</h4>
                <div className="stars">
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                  <i className="bi bi-star-fill"></i>
                </div>
                <p>
                  <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                  this is content paragraph
                  <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </SwiperSlide>
          </Swiper>
          <div className="swiper-pagination"></div>
        </div>
      </div>
    </section>
  );
};
export default Testimonial;
