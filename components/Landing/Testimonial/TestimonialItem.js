import React from "react";
import { SwiperSlide } from "swiper/react";

const TestimonialItem = (props) => {
  const { name, position, img: avatar, paragraph, altText } = props.testimonial;

  return (
    <SwiperSlide style={{ listStyle: "none" }}>
      <div className="testimonial-item">
        <img src={avatar} className="testimonial-img" alt={altText} />
        <h3>{name}</h3>
        <h4>{position}</h4>
        <div className="stars">
          <i className="bi bi-star-fill"></i>
          <i className="bi bi-star-fill"></i>
          <i className="bi bi-star-fill"></i>
          <i className="bi bi-star-fill"></i>
          <i className="bi bi-star-fill"></i>
        </div>
        <p>
          <i className="bx bxs-quote-alt-left quote-icon-left"></i>
          {paragraph}
          <i className="bx bxs-quote-alt-right quote-icon-right"></i>
        </p>
      </div>
    </SwiperSlide>
  );
};
export default TestimonialItem;
