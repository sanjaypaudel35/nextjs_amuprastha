import React, { useContext } from "react";
import StaticData from "Config/StaticDataConfig";
import { AboutUsContent } from "Config/AboutUsContent";
import { AnchorButton, PrimaryColorIcon } from "StyledComponents/Button.styled";
import useCheckLanguage from "hooks/useCheckLanguage";
import LanguageContext from "store/language-context";

const About = () => {
  const lang = useCheckLanguage();
  const languageCtx = useContext(LanguageContext);

  const bgImg = AboutUsContent[lang].youtubeThumbnail;
  const AboutUsList = AboutUsContent[lang].lists.map((list) => {
    return (
      <li key={list.content}>
        <PrimaryColorIcon className="bx bx-check-double"></PrimaryColorIcon>
        {list.content}
      </li>
    );
  });
  console.log("about us component lang", lang);
  console.log("context has", languageCtx.stateLang);
  return (
    <React.Fragment>
      <section id="about" className="about">
        <div className="container-fluid">
          <div className="row">
            <div
              className="col-lg-5 align-items-stretch video-box"
              style={{ backgroundImage: `url("${bgImg}")` }}
            >
              <AnchorButton
                href={AboutUsContent.youtubeLink}
                className="venobox play-btn mb-4"
                data-vbtype="video"
                data-autoplay="true"
              ></AnchorButton>
            </div>

            <div className="col-lg-7 d-flex flex-column justify-content-center align-items-stretch">
              <div className="content">
                <h3>
                  {StaticData[lang].aboutSectionHeadFirst}{" "}
                  <strong>{StaticData[lang].aboutSectionHeadSecond}</strong>
                </h3>
                <p>{AboutUsContent[lang].description1.firstParagraph}</p>
                <p className="fst-italic">
                  {AboutUsContent[lang].description1.secondParagraph}
                </p>

                <ul>{AboutUsList}</ul>
                <p>{AboutUsContent[lang].description2}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};
export default About;
