import React from "react";
import SiteConfig from "Config/SiteConfig";
import { PrimaryColorIcon } from "StyledComponents/Button.styled";

const TopBar = () => {
  return (
    <React.Fragment>
      <section
        id="topbar"
        className="d-flex align-items-center  topbar-transparent"
        style={{ backgroundColor: "black" }}
      >
        <div className="container-fluid container-xl d-flex align-items-center justify-content-center justify-content-lg-start">
          <PrimaryColorIcon className="bi bi-phone d-flex align-items-center">
            <span>{SiteConfig.contactNumber}</span>
          </PrimaryColorIcon>
          <PrimaryColorIcon className="bi bi-clock ms-4 d-none d-lg-flex align-items-center">
            <span>{SiteConfig.openingDayTime}</span>
          </PrimaryColorIcon>
        </div>
      </section>
    </React.Fragment>
  );
};
export default TopBar;
