import { useTheme } from "styled-components";
import { OnHoverPrimaryColorAnchor } from "StyledComponents/Text.styled";
const SpecialMenuName = (props) => {
  const theme = useTheme();
  const { id, name: special_menu_name } = props.item;
  const active_special_menu = props.activeSpecialMenu;
  const classToAdd = active_special_menu === id ? "active" : "";

  const activeStyle = {
    color: active_special_menu === id && theme.color.primaryColor,
    borderColor: active_special_menu === id && theme.color.primaryColor,
  };

  return (
    <li className="nav-item">
      <OnHoverPrimaryColorAnchor
        className={`nav-link ${classToAdd}`}
        style={activeStyle}
        id={id}
        onClick={props.onSpecialMenuClick}
      >
        {special_menu_name}
      </OnHoverPrimaryColorAnchor>
    </li>
  );
};
export default SpecialMenuName;
