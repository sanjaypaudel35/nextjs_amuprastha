import React from "react";
import classes from "./style.module.css";

const SpecialMenuItem = (props) => {
  const { title, description, img: menuImg } = props.item;
  return (
    <React.Fragment>
      <div
        className={`tab-pane active show ${
          props.animateMenu ? classes.fadeInCustomAnimation : ""
        }`}
        id="tab-1"
      >
        <div className="row">
          <div className="col-lg-8 details order-2 order-lg-1">
            <h3>{title}</h3>
            <p className="fst-italic">{description}</p>
          </div>
          <div className="col-lg-4 text-center order-1 order-lg-2">
            <img src={menuImg} alt="" className="img-fluid" />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default SpecialMenuItem;
