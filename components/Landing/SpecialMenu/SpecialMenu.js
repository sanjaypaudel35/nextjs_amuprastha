import React from "react";
import SiteConfig from "Config/SiteConfig";
import StaticData from "Config/StaticDataConfig";
import { useState, useEffect } from "react";
import SpecialMenuItem from "./SpeicalMenuItem";
import SpecialMenuName from "./SpecialMenuName";
import { SpecialMenusItems } from "Config/SpecialMenuContent";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import useCheckLanguage from "hooks/useCheckLanguage";

const SpecialMenu = () => {
  const lang = useCheckLanguage();
  const [activeSpecialMenu, setActiveSpecialMenu] = useState("sm1");
  const [animateMenu, setAnimateMenu] = useState(false);

  const specialMenuClickHandler = (e) => {
    setActiveSpecialMenu(e.currentTarget.id);
    activateAnimation();
  };

  const activateAnimation = () => {
    setAnimateMenu(true);
    // setTimeout(() => {
    //   setAnimateMenu(false);
    // }, 3000);
  };

  const renderSpecialMenuNames = SpecialMenusItems.map((item) => {
    return (
      <SpecialMenuName
        item={item}
        key={item.id}
        activeSpecialMenu={activeSpecialMenu}
        onSpecialMenuClick={specialMenuClickHandler}
      />
    );
  });

  const foundSpecialMenuContent = SpecialMenusItems.find((content) => {
    return content.id === activeSpecialMenu;
  });

  const renderSpecialMenuContent = (
    <SpecialMenuItem item={foundSpecialMenuContent} animateMenu={animateMenu} />
  );

  return (
    <React.Fragment>
      <section id="specials" className="specials">
        <div className="container">
          <div className="section-title">
            <h2>
              {StaticData[lang].specialMenuSectionHeadFirst}{" "}
              <PrimaryColorSpan>
                {StaticData[lang].specialMenuSectionHeadSecond}
              </PrimaryColorSpan>
            </h2>
            <p>{StaticData[lang].specialMenuSectionInfo}</p>
          </div>

          <div className="row">
            <div className="col-lg-3">
              <ul className="nav nav-tabs flex-column">
                {renderSpecialMenuNames}
              </ul>
            </div>
            <div className="col-lg-9 mt-4 mt-lg-0">
              <div className="tab-content">{renderSpecialMenuContent}</div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};
export default SpecialMenu;
