import React from "react";
import { PrimaryColorAnchor } from "StyledComponents/Text.styled";

const MenuItem = (props) => {
  const { name, price, description } = props.menu;
  return (
    <React.Fragment>
      <div className="col-lg-6 menu-item">
        <div className="menu-content">
          <PrimaryColorAnchor href="#">{name}</PrimaryColorAnchor>
          <span>{price}</span>
        </div>
        <div className="menu-ingredients">{description}</div>
      </div>
    </React.Fragment>
  );
};
export default MenuItem;
