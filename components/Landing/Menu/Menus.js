import React, { useState, useContext } from "react";
import MenuItem from "./MenuItem";
import MenuCategory from "./MenuCategory";
import { MenusContent } from "Config/MenusContent";
import StaticData from "Config/StaticDataConfig";
import LanguageContext from "store/language-context";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import useCheckLanguage from "hooks/useCheckLanguage";

const Menus = () => {
  const [activeMenu, setActiveMenu] = useState("m1");
  const lang = useCheckLanguage();

  const clickHandler = (e) => {
    setActiveMenu(e.currentTarget.id);
  };

  const MenuCategories = MenusContent[lang].map((category) => {
    return (
      <MenuCategory
        category={category}
        key={category.id}
        onCategoryClick={clickHandler}
        active={activeMenu}
      />
    );
  });

  const renderMenus = MenusContent[lang]
    .find((item) => {
      return item.id === activeMenu;
    })
    .items.map((menu) => {
      return <MenuItem menu={menu} key={menu.id} />;
    });

  return (
    <React.Fragment>
      <section id="menu" className="menu">
        <div className="container">
          <div className="section-title">
            <h2>
              {StaticData[lang].menuSectionHeadFirst}{" "}
              <PrimaryColorSpan>
                {StaticData[lang].menuSectionHeadSecond}
              </PrimaryColorSpan>
            </h2>
          </div>

          <div className="row">
            <div className="col-lg-12 d-flex justify-content-center">
              <ul id="menu-flters">{MenuCategories}</ul>
            </div>
          </div>

          <div className="row menu-container">{renderMenus}</div>
        </div>
      </section>
    </React.Fragment>
  );
};
export default Menus;
