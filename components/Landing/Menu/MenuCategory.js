import { ListWithBorder } from "StyledComponents/Button.styled.js";
import { useTheme } from "styled-components";

const MenuCategory = (props) => {
  const theme = useTheme();

  const { id: category_id, name: category_name } = props.category;

  let activeStyle;

  if (props.active === category_id) {
    activeStyle = {
      backgroundColor: theme.color.primaryColor,
      color: theme.color.textColorOverPrimaryColor,
    };
  }

  console.log(activeStyle);

  return (
    <ListWithBorder
      key={category_id}
      onClick={props.onCategoryClick}
      id={category_id}
      style={activeStyle}
    >
      {category_name}
    </ListWithBorder>
  );
};
export default MenuCategory;
