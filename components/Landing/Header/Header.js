import React, { useState } from "react";
import { MenuLists } from "Config/MenuLists";
import MenuItem from "./MenuItem/MenuItem.js";
import SiteConfig from "Config/SiteConfig";
import LanguageIcons from "./LanguageIcons";

import Link from "next/link";
import useCheckLanguage from "hooks/useCheckLanguage.js";

const Header = React.forwardRef((props, ref) => {
  const lang = useCheckLanguage();
  // const languageCtx = useContext(LanguageContext);

  // let localStorageLang;

  // useEffect(() => {
  //   if (typeof window !== "undefined") {
  //     // Perform localStorage action
  //     localStorageLang = localStorage.getItem("selected_lang");
  //   }
  // }, []);

  // const lang = localStorageLang ? localStorageLang : languageCtx.stateLang;

  // const { isMobile, DeviceIsMobile } = useCheckMobileView();
  const [showMobileNavbar, setToShowMobileNavbar] = useState(false);

  const toggleButttonClickhandler = () => {
    setToShowMobileNavbar(true);
  };

  const closeNavBarMobileHandler = () => {
    setToShowMobileNavbar(false);
  };
  const sortedMenus = MenuLists[lang].sort((a, b) => a.order - b.order);
  const Menus = sortedMenus.map((item) => {
    if (item.enable) {
      return <MenuItem item={item} key={item.title}></MenuItem>;
    }
  });
  return (
    <React.Fragment>
      {/*Header*/}
      <header
        ref={ref}
        id="header"
        className="fixed-top d-flex align-items-center header-transparent"
      >
        <div className="container-fluid container-xl d-flex align-items-center justify-content-between">
          <div className="logo me-auto">
            <h1>
              <Link href="/">{SiteConfig.SiteName}</Link>
            </h1>
          </div>
          <nav
            style={{ cursor: "pointer" }}
            id="navbar"
            className={`${
              showMobileNavbar && "navbar-mobile"
            } navbar order-last order-lg-0`}
          >
            <ul>{Menus}</ul>
            <i
              className="bi bi-list mobile-nav-toggle"
              onClick={toggleButttonClickhandler}
            ></i>
            {showMobileNavbar && (
              <button
                onClick={closeNavBarMobileHandler}
                style={{
                  top: "10px",
                  color: "red",
                  position: "fixed",
                  right: "10px",
                }}
              >
                close
              </button>
            )}
          </nav>
          {/*-- .navbar --*/}
          {/* <Button href="#book-a-table" className="book-a-table-btn scrollto">
            <a>Book a table</a>
          </Button> */}
          <div style={{ paddingLeft: "10px" }}>
            <LanguageIcons />
          </div>
        </div>
      </header>
      {/*-- End Header -- */
      /* ======= Hero Section ======= */}
    </React.Fragment>
  );
});
Header.displayName = "Header";
export default Header;
