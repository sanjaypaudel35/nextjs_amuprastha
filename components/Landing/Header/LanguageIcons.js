import { useContext, useEffect } from "react";
import LanguageContext from "store/language-context";

import LanguageIcon from "./LanguageIcon";

const language = [
  {
    id: "1",
    languageIcon: "en",
  },
  {
    id: "2",
    languageIcon: "ne",
  },
];

const LanguageIcons = () => {
  const languageCtx = useContext(LanguageContext);
  useEffect(() => {
    if (typeof window !== "undefined") {
      // Perform localStorage action
      if (localStorage.getItem("selected_lang")) {
        languageCtx.changeLang(
          localStorage.getItem("selected_lang"),
          localStorage.getItem("selected_lang_id")
        );
      }
    }
  }, []);

  const activeLang = languageCtx.stateLangId;

  const languageClickHandler = (e) => {
    localStorage.setItem("selected_lang", e.currentTarget.lang);
    localStorage.setItem("selected_lang_id", e.currentTarget.id);
    languageCtx.changeLang(e.currentTarget.lang, e.currentTarget.id);
  };

  const renderLangIcons = language.map((lang) => {
    return (
      <LanguageIcon
        activeLang={activeLang}
        lang={lang}
        key={lang.id}
        onLanguageClick={languageClickHandler}
      />
    );
  });

  return <>{renderLangIcons}</>;
};
export default LanguageIcons;
