import { SpanWithBorder } from "StyledComponents/Button.styled";
import { useTheme } from "styled-components";
const LanguageIcon = (props) => {
  const theme = useTheme();
  const activeLang = props.activeLang;
  let activeStyle;
  if (activeLang == props.lang.id) {
    activeStyle = {
      backgroundColor: theme.color.primaryColor,
      color: theme.color.textColorOverPrimaryColor,
      transition: "0.5s ease",
    };
  }
  return (
    <SpanWithBorder
      key={props.lang.id}
      onClick={props.onLanguageClick}
      id={props.lang.id}
      style={activeStyle}
      lang={props.lang.languageIcon}
    >
      {props.lang.languageIcon}
    </SpanWithBorder>
  );
};
export default LanguageIcon;
