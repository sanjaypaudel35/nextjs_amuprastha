import React, { useState } from "react";
import useCheckMobileView from "hooks/useChekMobileView";
import { OnHoverPrimaryColorAnchor } from "StyledComponents/Text.styled";

const ChildMenuItem = (props) => {
  const [showChildren, setShowChildren] = useState(false);
  const { isMobile } = useCheckMobileView();
  const { title, dropDown, href } = props.child;

  const showChildrenHandler = () => {
    alert(isMobile);
    if (isMobile) {
      setShowChildren((prevState) => {
        return !prevState;
      });
    }
  };

  let childrens;
  if (dropDown) {
    childrens = props.child.children.map((child) => {
      return (
        <li className={dropDown && "dropdown"} key={child.title}>
          <OnHoverPrimaryColorAnchor href={child.href}>
            {child.title}
          </OnHoverPrimaryColorAnchor>
        </li>
      );
    });
  }

  return (
    <React.Fragment>
      <li className={dropDown && "dropdown"} onClick={showChildrenHandler}>
        <a href={href}>
          {title}
          {dropDown && <i className="bi bi-chevron-right"></i>}
        </a>
        <ul className={showChildren && "dropdown-active"}>{childrens}</ul>
      </li>
    </React.Fragment>
  );
};
export default ChildMenuItem;
