import React, { useState } from "react";
import {
  OnHoverPrimaryColorAnchor,
  OnHoverPrimaryColorSpan,
  PrimaryColorAnchor,
} from "StyledComponents/Text.styled";
import ChildMenuItem from "./ChildMenuItem";
import useCheckMobileView from "hooks/useChekMobileView";
// import { useLocation } from "react-router-dom";
import Link from "next/link";
import { useRouter } from "next/router";
import { useTheme } from "styled-components";

const MenuItem = (props) => {
  const theme = useTheme();
  const url = useRouter();

  const [showChildren, setShowChildren] = useState(false);
  const { isMobile } = useCheckMobileView();
  const { title, href, dropDown, routeTo, enableRouting } = props.item;

  const showChildrenHandler = () => {
    if (isMobile) {
      setShowChildren((prevState) => {
        return !prevState;
      });
    }
  };

  let childrens;
  if (dropDown) {
    childrens = props.item.children.map((child) => {
      return <ChildMenuItem child={child} key={child.title} />;
    });
  }

  const activeMenuColor = {
    color: url.pathname == routeTo && theme.color.primaryColor,
  };

  return (
    <React.Fragment>
      <li className={dropDown && "dropdown"}>
        {enableRouting ? (
          <Link href={routeTo}>
            <a style={activeMenuColor}>
              <OnHoverPrimaryColorSpan>{title}</OnHoverPrimaryColorSpan>
            </a>
          </Link>
        ) : (
          <OnHoverPrimaryColorAnchor
            onClick={dropDown && showChildrenHandler}
            className="nav-link scrollto"
            href={href}
          >
            {title}
            {dropDown && <i className="bi bi-chevron-down"></i>}
          </OnHoverPrimaryColorAnchor>
        )}

        <ul className={showChildren && "dropdown-active"}>{childrens}</ul>
      </li>
    </React.Fragment>
  );
};
export default MenuItem;
