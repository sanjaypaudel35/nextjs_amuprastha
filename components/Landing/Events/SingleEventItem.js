import React from "react";
import { PrimaryColorIcon } from "StyledComponents/Button.styled";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";

const SingleEventItem = (props) => {
  const {
    title,
    price,
    description1,
    description2,
    img: eventImg,
    lists,
  } = props.event;

  const eventLists = lists.map((list) => {
    return (
      <li key={list.id}>
        <PrimaryColorIcon className="bi bi-check-circle"></PrimaryColorIcon>
        {list.paragraph}
      </li>
    );
  });
  return (
    <React.Fragment>
      {/* event content */}
      <div className="swiper-slide container">
        <div className="row event-item">
          <div className="col-lg-6">
            <img src={eventImg} className="img-fluid" alt="" />
          </div>
          <div className="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column align-items-start">
            <h3>
              <PrimaryColorSpan>{title}</PrimaryColorSpan>
            </h3>
            <div className="price">
              <p>
                <span>{price}</span>
              </p>
            </div>
            <p className="fst-italic text-left">{description1}</p>
            <ul>{eventLists}</ul>
            <p className="text-left">{description2}</p>
          </div>
        </div>
      </div>
      {/* end event content */}
    </React.Fragment>
  );
};
export default SingleEventItem;
