import React from "react";
import SiteConfig from "Config/SiteConfig";
import StaticData from "Config/StaticDataConfig";
import SingleEventItem from "./SingleEventItem";
import { EventsContent } from "Config/EventContent";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import ReactCarousel from "components/ReactCarousel";
import useCheckLanguage from "hooks/useCheckLanguage";

const Events = () => {
  const lang = useCheckLanguage();
  const renderEvents = EventsContent.map((event) => {
    return <SingleEventItem key={event.id} event={event} />;
  });
  return (
    <React.Fragment>
      <section id="events" className="events">
        <div className="container-fluid" style={{ position: "relative" }}>
          <div className="section-title">
            <h2>
              {StaticData[lang].eventSectionHeadFirst}{" "}
              <PrimaryColorSpan>
                {StaticData[lang].eventSectionHeadSecond}
              </PrimaryColorSpan>{" "}
              {StaticData[lang].eventSectionHeadThird}
            </h2>
          </div>

          <div className="events-slider swiper">
            <div className="swiper-wrapper">
              <ReactCarousel>{renderEvents}</ReactCarousel>
            </div>
            <div className="swiper-pagination"></div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};
export default Events;
