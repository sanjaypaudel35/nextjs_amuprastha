import React from "react";
import { useContext, useEffect } from "react";
import LanguageContext from "store/language-context";
import StaticData from "Config/StaticDataConfig";
import classes from "./ReactCarousel.module.css";
import { TransparentButton } from "StyledComponents/Button.styled";

const CarouselItem = (props) => {
  const languageCtx = useContext(LanguageContext);
  let localStorageLang;

  useEffect(() => {
    if (typeof window !== "undefined") {
      // Perform localStorage action
      localStorageLang = localStorage.getItem("selected_lang");
    }
  }, []);

  const lang = localStorageLang ? localStorageLang : languageCtx.stateLang;

  // alert(languageCtx.stateLang);

  const { title, description } = props.carousel;
  const bgImg = props.carousel.bgImg;

  return (
    <React.Fragment>
      <div style={{ height: "100vh" }}>
        <div
          style={{
            height: "100%",
            backgroundImage: `url("${bgImg}")`,
          }}
        >
          <div className={classes.reactCarouselContainer}>
            <div className={classes.reactCarouselContent}>
              <h2 className="animate__animated animate__fadeInDown">{title}</h2>
              <p className="animate__animated animate__fadeInUp">
                {description}
              </p>
              <div>
                <TransparentButton>
                  {StaticData[lang].carouselBtnText1}
                </TransparentButton>
                <TransparentButton>
                  {StaticData[lang].carouselBtnText2}
                </TransparentButton>
              </div>
            </div>
          </div>
        </div>
        {/* <p className="legend">Legend 1</p> */}
      </div>
    </React.Fragment>
  );
};
export default CarouselItem;
