import React from "react";
import ReactCarousel from "components/ReactCarousel.js";
import useCheckLanguage from "hooks/useCheckLanguage";
import { CarouselContent } from "Config/CarouselContent";
import CarouselItem from "./CarouselItem/CarouselItem";

const HeroCarousel = () => {
  const lang = useCheckLanguage();
  const CarouselItems = CarouselContent[lang].map((carousel) => {
    return <CarouselItem carousel={carousel} key={carousel.id} />;
  });

  return (
    <React.Fragment>
      <section id="hero">
        <div className="hero-container">
          <div className="blankoverlay_on_mobile"></div>
          <ReactCarousel>{CarouselItems}</ReactCarousel>
        </div>
      </section>
    </React.Fragment>
  );
};
export default HeroCarousel;
