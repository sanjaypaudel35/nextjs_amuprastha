import React from "react";
import SiteConfig from "Config/SiteConfig";
import StaticData from "Config/StaticDataConfig";
import SocialIcon from "./SocialIcon";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import useCheckLanguage from "hooks/useCheckLanguage";

const Footer = () => {
  const lang = useCheckLanguage();

  const renderSocialIcons = SiteConfig.socialIcons.map((social) => {
    return <SocialIcon social={social} key={social.id} />;
  });
  return (
    <React.Fragment>
      <footer id="footer">
        <div className="container">
          <h3>
            <PrimaryColorSpan>{SiteConfig.FooterHeading}</PrimaryColorSpan>
          </h3>
          <p>{StaticData[lang].footerParagraph}</p>
          <div className="social-links">{renderSocialIcons}</div>
          <div className="copyright">
            &copy; Copyright
            <strong>
              <span>{SiteConfig.FooterHeading}</span>
            </strong>
            . All Rights Reserved
          </div>
          <div className="credits"></div>
        </div>
      </footer>
    </React.Fragment>
  );
};
export default Footer;
