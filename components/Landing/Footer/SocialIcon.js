import React from "react";
import { OnHoverPrimaryColorAnchorButton } from "StyledComponents/Button.styled";

const SocialIcon = (props) => {
  const linkTarget = props.social.newTab ? "_blank" : "_self";
  return (
    <React.Fragment>
      {props.social.enable && (
        <OnHoverPrimaryColorAnchorButton
          href={props.social.link}
          className={props.social.anchor_class}
          target={linkTarget}
        >
          <i className={props.social.icon_class}></i>
        </OnHoverPrimaryColorAnchorButton>
      )}
    </React.Fragment>
  );
};
export default SocialIcon;
