import React from "react";
import ReactCarousel from "components/ReactCarousel";
import StaticData from "Config/StaticDataConfig";
import ChefConfig from "Config/ChefConfig";
import IndividualChef from "./IndividualChef";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import useCheckLanguage from "hooks/useCheckLanguage";

const Chefs = () => {
  const lang = useCheckLanguage();
  const renderrChefs = ChefConfig.ChefContent.map((chef) => {
    return <IndividualChef chef={chef} key={chef.id} />;
  });
  return (
    <React.Fragment>
      <section id="chefs" className="chefs">
        <div className="container">
          <div className="section-title">
            <h2>
              {StaticData[lang].chefSectionHeadFirst}{" "}
              <PrimaryColorSpan>
                {StaticData[lang].chefSectionHeadSecond}
              </PrimaryColorSpan>
            </h2>
            <p>{StaticData[lang].chefSectionInfo}</p>
          </div>

          <div className="row">{renderrChefs}</div>
        </div>
      </section>
    </React.Fragment>
  );
};
export default Chefs;
