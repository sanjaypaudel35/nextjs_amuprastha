import React from "react";
import ChefSocialIcon from "./ChefSocialIcon";

const IndividualChef = (props) => {
  const { name, position, img: avatar, alt, social } = props.chef;
  const renderSocialIcons = social.map((social) => {
    return <ChefSocialIcon icons={social} key={social.id} />;
  });
  return (
    <React.Fragment>
      <div className="col-lg-4 col-md-6">
        <div className="member">
          <div className="pic">
            <img src={avatar} className="img-fluid" alt={alt} />
          </div>
          <div className="member-info">
            <h4>{name}</h4>
            <span>{position}</span>
            <div className="social">{renderSocialIcons}</div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default IndividualChef;
