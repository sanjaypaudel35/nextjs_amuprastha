import React from "react";
import { OnHoverPrimaryColorAnchor } from "StyledComponents/Text.styled";

const ChefSocialIcon = (props) => {
  const { link, iconClass, enable, newTab } = props.icons;
  const linkTarget = newTab ? "_blank" : "_self";
  return (
    <React.Fragment>
      {enable && (
        <OnHoverPrimaryColorAnchor href={link} target={linkTarget}>
          <i className={iconClass}></i>
        </OnHoverPrimaryColorAnchor>
      )}
    </React.Fragment>
  );
};
export default ChefSocialIcon;
