import React from "react";
import SiteConfig from "Config/SiteConfig";
import StaticData from "Config/StaticDataConfig";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import {
  Button,
  PirmaryColorCircularIcon,
} from "StyledComponents/Button.styled";
import useCheckLanguage from "hooks/useCheckLanguage";

const Contact = () => {
  const lang = useCheckLanguage();
  return (
    <React.Fragment>
      <section id="contact" className="contact">
        <div className="container">
          <div className="section-title">
            <h2>
              <PrimaryColorSpan>
                {StaticData[lang].contactSectionHeadFirst}
              </PrimaryColorSpan>
              {StaticData[lang].contactSectionHeadSecond}
            </h2>
            <p>{StaticData[lang].contactSectionInfo}</p>
          </div>
        </div>

        <div className="map">
          <iframe
            style={{ border: "0px", width: "100%", height: "350px" }}
            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621"
            frameBorder="0"
            allowFullScreen
          ></iframe>
        </div>

        <div className="container mt-5">
          <div className="info-wrap">
            <div className="row">
              <div className="col-lg-3 col-md-6">
                <PirmaryColorCircularIcon className="bi bi-geo-alt" />
                <div style={{ paddingLeft: "60px" }}>
                  <h4>{StaticData[lang].textLocation}:</h4>
                  <p>
                    {StaticData[lang].textLocationFirstLine}
                    <br />
                    {StaticData[lang].textLocationSecondLine}
                  </p>
                </div>
              </div>

              <div className="col-lg-3 col-md-6  mt-4 mt-lg-0">
                <PirmaryColorCircularIcon className="bi bi-clock" />
                <div style={{ paddingLeft: "60px" }}>
                  <h4>{StaticData[lang].textOpenHours}:</h4>
                  <p>
                    {StaticData[lang].openPeriod}
                    <br />
                    {StaticData[lang].openTime}
                  </p>
                </div>
              </div>

              <div className="col-lg-3 col-md-6 mt-4 mt-lg-0">
                <PirmaryColorCircularIcon className="bi bi-envelope" />
                <div style={{ paddingLeft: "60px" }}>
                  <h4>{StaticData[lang].textEmail}:</h4>
                  <p>
                    info@example.com
                    <br />
                    contact@example.com
                  </p>
                </div>
              </div>

              <div className="col-lg-3 col-md-6 mt-4 mt-lg-0">
                <PirmaryColorCircularIcon className="bi bi-phone" />
                <div style={{ paddingLeft: "60px" }}>
                  <h4>{StaticData[lang].textCall}:</h4>
                  <p>
                    +1 5589 55488 51
                    <br />
                    +1 5589 22475 14
                  </p>
                </div>
              </div>
            </div>
          </div>

          <form
            action="forms/contact.php"
            method="post"
            role="form"
            className="php-email-form"
          >
            <div className="row">
              <div className="col-md-6 form-group">
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  id="name"
                  placeholder={StaticData[lang].namePlaceHolder}
                  required
                />
              </div>
              <div className="col-md-6 form-group mt-3 mt-md-0">
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  id="email"
                  placeholder={StaticData[lang].emailPlaceHolder}
                  required
                />
              </div>
            </div>
            <div className="form-group mt-3">
              <input
                type="text"
                className="form-control"
                name="subject"
                id="subject"
                placeholder={StaticData[lang].subjectPlaceHolder}
                required
              />
            </div>
            <div className="form-group mt-3">
              <textarea
                className="form-control"
                name="message"
                rows="5"
                placeholder={StaticData[lang].messagePlaceHolder}
                required
              ></textarea>
            </div>
            <div className="my-3">
              <div className="loading">Loading</div>
              <div className="error-message"></div>
              <div className="sent-message">
                Your message has been sent. Thank you!
              </div>
            </div>
            <div className="text-center">
              <Button type="submit">
                {StaticData[lang].contactFormButtonText}
              </Button>
            </div>
          </form>
        </div>
      </section>
    </React.Fragment>
  );
};
export default Contact;
