import React from "react";

const GalleryItem = (props) => {
  const img = props.gallery.name;
  const alt = props.gallery.alt;

  const imageClickHandler = () => {
    props.onImageClick(props.index);
  };
  return (
    <React.Fragment>
      <div className="col-lg-3 col-md-4">
        <div className="gallery-item" onClick={imageClickHandler}>
          {" "}
          {/* <a href={img} class="gallery-lightbox"> */}
          <img src={img} alt={alt} className="img-fluid" />
          {/* </a> */}
        </div>
      </div>
    </React.Fragment>
  );
};
export default GalleryItem;
