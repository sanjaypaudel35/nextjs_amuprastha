import React from "react";
import StaticData from "Config/StaticDataConfig";
import GalleryItem from "./GalleryItem";
import GalleryConfig from "Config/GalleryConfig";

import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import useCheckLanguage from "hooks/useCheckLanguage";
import Gallery from "components/Gallery";
import useGallery from "hooks/useGallery";

const PhotoGallery = () => {
  const ourGallery = useGallery(GalleryConfig.GalleryContent);
  const lang = useCheckLanguage();

  const renderGalleryImages = GalleryConfig.GalleryContent.map(
    (gallery, index) => {
      return (
        <GalleryItem
          gallery={gallery}
          key={gallery.id}
          onImageClick={ourGallery.showGalleryImage}
          index={index}
        />
      );
    }
  );

  return (
    <React.Fragment>
      <Gallery ourGallery={ourGallery} />

      <section id="gallery" className="gallery">
        <div className="container-fluid">
          <div className="section-title">
            <h2>
              {StaticData[lang].gallerySectionHeadFirst}{" "}
              <PrimaryColorSpan>
                {StaticData[lang].gallerySectionHeadSecond}
              </PrimaryColorSpan>
            </h2>
            <p>{StaticData[lang].gallerySectionInfo}</p>
          </div>
          <div className="row g-0">{renderGalleryImages}</div>
        </div>
      </section>
    </React.Fragment>
  );
};
export default PhotoGallery;
