import React from "react";
import styled from "styled-components";
import StaticData from "Config/StaticDataConfig";
import { WhyUsContent } from "Config/WhyUsContent";
import { PrimaryColorSpan } from "StyledComponents/Text.styled";
import useCheckLanguage from "hooks/useCheckLanguage";

const WhyUs = () => {
  const lang = useCheckLanguage();

  const WhyUsLists = WhyUsContent[lang].map((item) => {
    return <WhyUsList item={item} key={item.id} />;
  });
  return (
    <React.Fragment>
      <section id="why-us" className="why-us">
        <div className="container">
          <div className="section-title">
            <h2>
              {StaticData[lang].whyUsSectionHeadFirst}{" "}
              <PrimaryColorSpan>
                {StaticData[lang].whyUsSectionHeadSecond}
              </PrimaryColorSpan>
            </h2>
            <p>{StaticData[lang].whyUsSectionInfo}</p>
          </div>

          <div className="row">{WhyUsLists}</div>
        </div>
      </section>
    </React.Fragment>
  );
};
export default WhyUs;

const WhyUsList = (props) => {
  const { id: number, title, description } = props.item;
  return (
    <React.Fragment>
      <div className="col-lg-4 mt-4 mt-lg-0">
        <BgPrimaryColorOnHoverDIV className="box">
          <PrimaryColorSpan>{number}</PrimaryColorSpan>
          <h4>{title}</h4>
          <p>{description}</p>
        </BgPrimaryColorOnHoverDIV>
      </div>
    </React.Fragment>
  );
};

const BgPrimaryColorOnHoverDIV = styled.div`
  &:hover {
    background: ${(props) => props.theme.color.primaryColor} !important;
  }
`;
