import Head from "next/head";
const MetaHead = (props) => {
  const { keyword, description, title } = props.meta;
  return (
    <Head>
      <title>{title}</title>
      <meta content={description} name="description" />
      <meta content={keyword} name="keywords" />

      <link
        href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i"
        rel="stylesheet"
      />

      <link
        href="./assets/vendor/animate.css/animate.min.css"
        rel="stylesheet"
      />
      <link
        href="./assets/vendor/bootstrap/css/bootstrap.min.css"
        rel="stylesheet"
      />
      <link
        href="./assets/vendor/bootstrap-icons/bootstrap-icons.css"
        rel="stylesheet"
      />
      <link
        href="./assets/vendor/boxicons/css/boxicons.min.css"
        rel="stylesheet"
      />
      <link
        href="./assets/vendor/glightbox/css/glightbox.min.css"
        rel="stylesheet"
      />
      <link
        href="./assets/vendor/swiper/swiper-bundle.min.css"
        rel="stylesheet"
      />

      <link href="./assets/css/style.css" rel="stylesheet" />
    </Head>
  );
};
export default MetaHead;
