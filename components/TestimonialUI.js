import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, {
  Navigation,
  Pagination,
  Autoplay,
  Virtual,
} from "swiper/core";

// import "../styles.css";

SwiperCore.use([Navigation, Pagination, Autoplay, Virtual]);
const TestimonialUI = (props) => {
  return (
    <section id="testimonials" className="testimonials">
      <div classNameName="container position-relative">
        <div
          className="testimonials-slider swiper"
          data-aos="fade-up"
          data-aos-delay="100"
        >
          <Swiper
            id="swiper"
            virtual
            slidesPerView={3}
            // slidesPerColumn={2}
            // slidesPerColumnFill="row"
            spaceBetween={30}
            // slidesPerGroup={2}
            autoplay
            // loop
            //   onReachEnd={() => {
            //     console.log("reach end");
            //     const tmp = slides.unshift();
            //     slides.push(tmp);
            //   }}
            navigation
            pagination
          >
            {props.children}
          </Swiper>
          {/* <div className="swiper-pagination"></div> */}
        </div>
      </div>
    </section>
  );
};
export default TestimonialUI;
