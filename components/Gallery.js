import React from "react";
import { SpanWithBorder } from "StyledComponents/Button.styled";
import classes from "./Gallery.module.css";
import Image from "next/image";

const Gallery = (props) => {
  const activeImageIndex = props.ourGallery.openedImageIndex;

  const allImages = props.ourGallery.galleryData;

  const imgToShow = allImages[activeImageIndex];

  const img = imgToShow.name;
  return (
    <React.Fragment>
      {props.ourGallery.isModalActive && (
        <div>
          <div className={classes.backdrop}>
            <div className={classes.modalOnBackDrop}>
              <div className={classes.modalCloseBtn}>
                <div className={classes.imageChangeBtnWrapper}>
                  {allImages[activeImageIndex - 1] && (
                    <SpanWithBorder
                      onClick={props.ourGallery.prevImageHandler}
                      className={classes.imageChangeBtn}
                    >
                      Prev
                    </SpanWithBorder>
                  )}
                  {allImages[activeImageIndex + 1] && (
                    <SpanWithBorder
                      onClick={props.ourGallery.nextImageHandler}
                      className={classes.imageChangeBtn}
                    >
                      Next
                    </SpanWithBorder>
                  )}
                </div>

                <SpanWithBorder onClick={props.ourGallery.closeModal}>
                  close
                </SpanWithBorder>
              </div>
              {/* content */}
              <div className={classes.modalContent}>
                <img
                  src={img}
                  style={{
                    height: "100%",
                    width: "100%",
                    objectFit: "contain",
                  }}
                />
                <span style={{ color: "white", opacity: "0.6" }}>
                  {activeImageIndex + 1} th image from {allImages.length}
                </span>
                <p
                  style={{
                    textAlign: "center",
                    color: "white",
                    opacity: "0.6",
                  }}
                >
                  {allImages[activeImageIndex].description}
                </p>
              </div>

              {/* end of content */}
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
export default Gallery;
