import React, { Component } from "react";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

const ReactCarousel = (props) => {
  return (
    <Carousel
      showStatus={false}
      emulateTouch
      showThumbs={false}
      autoPlay={true}
      autoFocus={false}
    >
      {props.children}
    </Carousel>
  );
};
export default ReactCarousel;
