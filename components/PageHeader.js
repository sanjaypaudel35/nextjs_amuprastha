import React, { useState, useContext, useEffect } from "react";
import SiteConfig from "Config/SiteConfig";
import LanguageIcons from "components/Landing/Header/LanguageIcons";
import useCheckMobileView from "hooks/useChekMobileView";
import LanguageContext from "store/language-context";
import Link from "next/link";
import { MenuLists } from "Config/MenuLists";
import MenuItem from "components/Landing/Header/MenuItem/MenuItem.js";
import useCheckLanguage from "hooks/useCheckLanguage";

const PageHeader = () => {
  const lang = useCheckLanguage();

  // const { isMobile, DeviceIsMobile } = useCheckMobileView();
  const [showMobileNavbar, setToShowMobileNavbar] = useState(false);

  const toggleButttonClickhandler = () => {
    setToShowMobileNavbar(true);
  };

  const closeNavBarMobileHandler = () => {
    setToShowMobileNavbar(false);
  };

  const sortedMenus = MenuLists[lang].sort((a, b) => a.order - b.order);
  const Menus = sortedMenus.map((item) => {
    if (item.enable) {
      return <MenuItem item={item} key={item.title}></MenuItem>;
    }
  });
  return (
    <React.Fragment>
      <header id="header" className="d-flex align-items-center">
        <div className="container-fluid container-xl d-flex align-items-center justify-content-between">
          <div className="logo me-auto">
            <h1>
              <Link href="/">{SiteConfig.SiteName}</Link>
            </h1>
          </div>
          <nav
            id="navbar"
            style={{ zIndex: "999" }}
            className={`${
              showMobileNavbar && "navbar-mobile"
            } navbar order-last order-lg-0`}
          >
            <ul>{Menus}</ul>
            <i
              className="bi bi-list mobile-nav-toggle"
              onClick={toggleButttonClickhandler}
            ></i>
            {showMobileNavbar && (
              <button
                onClick={closeNavBarMobileHandler}
                style={{
                  top: "10px",
                  backgroundColor: "red",
                  position: "fixed",
                  right: "10px",
                }}
              >
                close
              </button>
            )}
          </nav>
          <div style={{ paddingLeft: "10px" }}>
            <LanguageIcons />
          </div>
        </div>
      </header>
    </React.Fragment>
  );
};
export default PageHeader;
