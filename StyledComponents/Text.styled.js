import styled from "styled-components";

const PrimaryColorSpan = styled.span`
  color: ${(props) => props.theme.color.primaryColor} !important;
`;
const PrimaryColorAnchor = styled.a`
  color: ${(props) => props.theme.color.primaryColor} !important;
  cursor: pointer;

  &:hover {
    cursor: pointer;
    color: ${(props) => props.theme.color.primaryColor} !important;
  }
`;

const OnHoverPrimaryColorAnchor = styled.a`
  cursor: pointer;

  &:hover {
    cursor: pointer;
    color: ${(props) => props.theme.color.primaryColor} !important;
  }
`;
const OnHoverPrimaryColorSpan = styled.span`
  cursor: pointer;

  &:hover {
    cursor: pointer;
    color: ${(props) => props.theme.color.primaryColor} !important;
  }
`;
export {
  PrimaryColorSpan,
  PrimaryColorAnchor,
  OnHoverPrimaryColorAnchor,
  OnHoverPrimaryColorSpan,
};
