import styled from "styled-components";

const Button = styled.button`
  background: ${(props) => props.theme.color.primaryColor} !important;
  padding: 10px 24px !important;
  color: #fff !important;
  transition: 0.4s !important;
  border-radius: 50px !important;

  &:hover {
    background: ${(props) => props.theme.color.hoverOnPrimaryColor} !important;
  }
`;

const TransparentButton = styled.button`
  font-weight: 600;
  font-size: 13px;
  letter-spacing: 1px;
  text-transform: uppercase;
  display: inline-block;
  padding: 12px 30px;
  border-radius: 50px;
  transition: 0.5s;
  line-height: 1;
  margin: 0 10px;
  -webkit-animation-delay: 0.8s;
  background: transparent;
  animation-delay: 0.8s;
  color: #fff;
  border: 2px solid ${(props) => props.theme.color.primaryColor};

  &:hover {
    background: ${(props) => props.theme.color.primaryColor} !important;
  }
`;

const ListWithBorder = styled.li`
  border: 2px solid ${(props) => props.theme.color.primaryColor} !important;

  &:hover {
    background: ${(props) => props.theme.color.primaryColor} !important;
  }
`;

const SpanWithBorder = styled.span`
  border: 1px solid ${(props) => props.theme.color.primaryColor} !important;
  margin-left: 5px;
  border-radius: 3px;
  color: white;
  padding: 5px;
  &:hover {
    cursor: pointer !important;
  }
`;

const PrimaryColorIcon = styled.i`
  color: ${(props) => props.theme.color.primaryColor} !important;
`;

const AnchorButton = styled.a`
  background: ${(props) => props.theme.color.primaryColor} !important;

  &:hover {
    background: ${(props) => props.theme.color.primaryColor} !important;
  }
`;

const OnHoverPrimaryColorAnchorButton = styled.a`
  &:hover {
    background: ${(props) => props.theme.color.primaryColor} !important;
  }
`;
const PirmaryColorCircularIcon = styled.i`
  font-size: 20px;
  color: ${(props) => props.theme.color.primaryColor} !important;
  float: left;
  width: 44px;
  height: 44px;
  background: #fff6e8;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50px;
  transition: all 0.3s ease-in-out;
  &:hover {
    background: ${(props) => props.theme.color.primaryColor} !important;
    color: white !important;
  }
`;
export {
  Button,
  TransparentButton,
  ListWithBorder,
  PrimaryColorIcon,
  AnchorButton,
  OnHoverPrimaryColorAnchorButton,
  SpanWithBorder,
  PirmaryColorCircularIcon,
};
