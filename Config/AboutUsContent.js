import SiteConfig from "Config/SiteConfig";
const path = SiteConfig.imagePath.systemImage;
export const AboutUsContent = {
  en: {
    youtubeLink: "https://www.youtube.com/watch?v=jDDaplaOz7Q",
    youtubeThumbnail: path + "about.jpg",
    description1: {
      firstParagraph:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      secondParagraph:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
    lists: [
      {
        content: "This is list 1.",
      },
      {
        content: "This is list 2.",
      },
      {
        content: "This is list 3",
      },
    ],
    description2:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
  },
  ne: {
    youtubeLink: "https://www.youtube.com/watch?v=jDDaplaOz7Q",
    youtubeThumbnail: path + "about.jpg",
    description1: {
      firstParagraph:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
      secondParagraph:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
    lists: [
      {
        content: "यो पहिलो अनुच्छेद सूची हो |",
      },
      {
        content: "यो सूची अनुच्छेद दोस्रो हो |",
      },
      {
        content: "यो सूची अनुच्छेद तेस्रो हो |",
      },
    ],
    description2:
      "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
  },
};
