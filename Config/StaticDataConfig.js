const StaticData = {
  en: {
    carouselBtnText1: "Our Menu",
    carouselBtnText2: "Book A Table",

    aboutSectionHeadFirst: "About Us",
    aboutSectionHeadSecond: "Amuprastha Restro and Boutique",

    menuSectionHeadFirst: "Check Our Tasty",
    menuSectionHeadSecond: "Menus",

    whyUsSectionHeadFirst: "Why choose",
    whyUsSectionHeadSecond: "Our Restaurant",
    whyUsSectionInfo:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",

    specialMenuSectionHeadFirst: "Check our",
    specialMenuSectionHeadSecond: "Specials",
    specialMenuSectionInfo:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",

    eventSectionHeadFirst: "Organize Your",
    eventSectionHeadSecond: "Events",
    eventSectionHeadThird: "in our Restaurant",

    bookATableSectionHeadFirst: "Book a",
    bookATableSectionHeadSecond: "Table",
    bookATableSectionInfo:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",

    bookATableFormButtonText: "Send Message",

    gallerySectionHeadFirst: "Some photo of",
    gallerySectionHeadSecond: " Our Restaurant",
    gallerySectionInfo:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",

    chefSectionHeadFirst: "Our professional",
    chefSectionHeadSecond: "Chefs",
    chefSectionInfo:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",

    contactSectionHeadFirst: "Contact",
    contactSectionHeadSecond: "Us",
    contactSectionInfo:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",

    contactFormButtonText: "Send Message",

    textLocation: "Location",
    textLocationFirstLine: "A108 Adam Street",
    textLocationSecondLine: "New York, NY 535022",

    textOpenHours: "Open Hours",
    openPeriod: "Monday-Sataurday",
    openTime: "9am- 6pm",
    namePlaceHolder: "Enter Your Name",
    emailPlaceHolder: "Enter Your Email Address",
    subjectPlaceHolder: "Enter Your Subject",
    messagePlaceHolder: "Your Message",
    textEmail: "Email",
    textCall: "Call",

    footerParagraph:
      "Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.",
  },
  ne: {
    carouselBtnText1: "हाम्रो मेनु",
    carouselBtnText2: "टेबल बुक गर्नुहोस्",

    aboutSectionHeadFirst: "हाम्रोबारे",
    aboutSectionHeadSecond: "अमुप्रस्थ रेस्ट्रो एण्ड बुटिक",

    menuSectionHeadFirst: "हाम्रो स्वादिष्ट मेनु",
    menuSectionHeadSecond: "जाँच गर्नुहोस्",

    whyUsSectionHeadFirst: "किन छनौट गर्ने",
    whyUsSectionHeadSecond: "हाम्रो रेस्टुरेन्ट",
    whyUsSectionInfo:
      "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",

    specialMenuSectionHeadFirst: "हाम्रो विशेष खाना",
    specialMenuSectionHeadSecond: "जाँच गर्नुहोस्",
    specialMenuSectionInfo:
      "Lorem Ipsum को खण्डहरूको धेरै भिन्नताहरू उपलब्ध छन्, तर बहुमतले कुनै न कुनै रूपमा परिवर्तनको सामना गरेको छ।",

    eventSectionHeadFirst: "हाम्रो रेस्टुरेन्टमा तपाईंको",
    eventSectionHeadSecond: "कार्यक्रम",
    eventSectionHeadThird: "व्यवस्थापन गर्नुहोस्",

    bookATableSectionHeadFirst: "Book a",
    bookATableSectionHeadSecond: "Table",
    bookATableSectionInfo:
      "Lorem Ipsum को खण्डहरूको धेरै भिन्नताहरू उपलब्ध छन्, तर बहुमतले कुनै न कुनै रूपमा परिवर्तनको सामना गरेको छ।",

    bookATableFormButtonText: "Send Message",

    gallerySectionHeadFirst: "हाम्रो रेस्टुरेन्टका केही ",
    gallerySectionHeadSecond: "तस्बिरहरू",
    gallerySectionInfo:
      "Lorem Ipsum को खण्डहरूको धेरै भिन्नताहरू उपलब्ध छन्, तर बहुमतले कुनै न कुनै रूपमा परिवर्तनको सामना गरेको छ।",

    chefSectionHeadFirst: "हाम्रा व्यावसायिक",
    chefSectionHeadSecond: "शेफहरू",
    chefSectionInfo:
      "Lorem Ipsum को खण्डहरूको धेरै भिन्नताहरू उपलब्ध छन्, तर बहुमतले कुनै न कुनै रूपमा परिवर्तनको सामना गरेको छ।",

    contactSectionHeadFirst: "हामीलाई",
    contactSectionHeadSecond: "सम्पर्क गर्नुहोस",
    contactSectionInfo:
      "Lorem Ipsum को खण्डहरूको धेरै भिन्नताहरू उपलब्ध छन्, तर बहुमतले कुनै न कुनै रूपमा परिवर्तनको सामना गरेको छ।",

    contactFormButtonText: "सन्देश पठाउनुहोस्",

    textLocation: "स्थान",
    textLocationFirstLine: "A108 एडम स्ट्रीट",
    textLocationSecondLine: "न्यूयोर्क, NY 535022",

    textOpenHours: "खुल्ने समय",
    openPeriod: "सोमबार देखि शनिबार सम्म",
    openTime: "बिहान 9 बजे देखि साँझ 6 बजे सम्म",
    textEmail: "इमेल",
    textCall: "कल गर्नुहोस्",

    namePlaceHolder: "आफ्नो नाम लेख्नुहोस्",
    emailPlaceHolder: "आफ्नो इमेल ठेगाना लेख्नुहोस्",
    subjectPlaceHolder: "आफ्नो विषय लेख्नुहोस्",
    messagePlaceHolder: "आफ्नो सन्देश लेख्नुहोस्",

    footerParagraph:
      "र तिनीहरू उहाँ वा तिनीहरूबाट उम्कन सक्ने कोही बाहेक अरू केही होइनन्। ठूलो शारीरिक परिश्रमको उडान छनौट गर्नु प्रायः सबैभन्दा सहज कुरा हो",
  },
};
export default StaticData;
