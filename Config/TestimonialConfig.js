import SiteConfig from "Config/SiteConfig";
const path = SiteConfig.imagePath.testimonialImage;
const Testimonialcontent = [
  {
    id: "t1",
    name: "Saul Goodman",
    position: "Ceo & Founder",
    img: path + "testimonials-1.jpg",
    altText: "Saul Goodman",
    paragraph:
      "Proin iaculis purus consequat sem cure digni ssim donec" +
      "porttitora entum suscipit rhoncus. Accusantium quam," +
      "ultricies eget id, aliquam eget nibh et. Maecen aliquam," +
      "risus at semper.",
  },
  {
    id: "t2",
    name: "Saul Goodman",
    position: "Founder",
    img: path + "testimonials-2.jpg",
    altText: "Saul Goodman",
    paragraph:
      "Proin iaculis purus consequat sem cure digni ssim donec" +
      "porttitora entum suscipit rhoncus. Accusantium quam," +
      "ultricies eget id, aliquam eget nibh et. Maecen aliquam," +
      "risus at semper.",
  },
  {
    id: "t3",
    name: "Saul Goodman",
    position: "Seo expert and digital marketter",
    img: path + "testimonials-3.jpg",
    altText: "Saul Goodman",
    paragraph:
      "Proin iaculis purus consequat sem cure digni ssim donec" +
      "porttitora entum suscipit rhoncus. Accusantium quam," +
      "ultricies eget id, aliquam eget nibh et. Maecen aliquam," +
      "risus at semper.",
  },
  {
    id: "t4",
    name: "Saul Goodman",
    position: "Manager",
    img: path + "testimonials-4.jpg",
    altText: "Saul Goodman",
    paragraph:
      "Proin iaculis purus consequat sem cure digni ssim donec" +
      "porttitora entum suscipit rhoncus. Accusantium quam," +
      "ultricies eget id, aliquam eget nibh et. Maecen aliquam," +
      "risus at semper.",
  },
];

export default {
  Testimonialcontent,
};
