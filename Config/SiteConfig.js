const socialIcons = [
  {
    id: "social1",
    name: "facebook",
    icon_class: "bx bxl-facebook",
    anchor_class: "facebook",
    enable: true,
    newTab: true,
    link: "#",
  },
  {
    id: "social2",
    name: "twitter",
    icon_class: "bx bxl-twitter",
    anchor_class: "twitter",
    enable: true,
    newTab: true,
    link: "#",
  },
  {
    id: "social3",
    name: "instagram",
    icon_class: "bx bxl-instagram",
    anchor_class: "instagram",
    enable: true,
    newTab: true,
    link: "#",
  },
  {
    id: "social4",
    name: "linkedin",
    icon_class: "bx bxl-linkedin",
    anchor_class: "linkedin",
    enable: true,
    newTab: true,
    link: "#",
  },
];

const SiteName = "Amuprastha";
const contactNumber = "+977 562829 37728";
const openingDayTime = "Mon-Sat: 9:00 AM - 6:00 PM";
const FooterHeading = SiteName;

const Theme = {
  color: {
    primaryColor: "#fc6e12",
    secondaryColor: "",

    textColorOverPrimaryColor: "white",
    textColorOverSecondaryColor: "",

    hoverOnPrimaryColor: "#ffb03b",
    hoverOnSecondaryColor: "",

    primaryTextColor: "",

    secondaryTextColor: "",
  },
};

const imagePath = {
  chefImage: "assets/img/chefs/",
  galleryImage: "/assets/img/gallery/",
  sliderImage: "/assets/img/slide/",
  testimonialImage: "/assets/img/testimonials/",
  eventsImage: "/assets/img/events/",
  specialMenuImage: "/assets/img/specialmenus/",

  systemImage: "/assets/img/",
};

const site_lang = "en";

// const local_storage_site_lang = localStorage.getItem("selected_lang");

export default {
  socialIcons,
  FooterHeading,

  contactNumber,
  SiteName,
  openingDayTime,
  Theme,
  site_lang,
  // local_storage_site_lang,
  imagePath,
};
