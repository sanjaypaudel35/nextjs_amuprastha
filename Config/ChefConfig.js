import SiteConfig from "Config/SiteConfig";
const path = SiteConfig.imagePath.chefImage;
const socialIconConfig = {
  facebook: "bi bi-facebook",
  twitter: "bi bi-twitter",
  instagram: "bi bi-instagram",
  linkdin: "bi bi-linkedin",
  newTab: true,
};

const ChefContent = [
  {
    id: "c1",
    name: "Walter White",
    position: "Master Chef",
    img: path + "chefs-1.jpg",
    alt: "chefs-1",
    social: [
      {
        id: "s1",
        socialType: "facebook",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.facebook,
      },
      {
        id: "s2",
        socialType: "twitter",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.twitter,
      },
      {
        id: "s3",
        socialNamme: "instagram",
        enable: true,
        link: "https://www.instagram.com/jaysanpaudel/",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.instagram,
      },

      {
        id: "s4",
        socialName: "linkdin",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.linkdin,
      },
    ],
  },

  {
    id: "c2",
    name: "Walter White",
    position: "Master Chef",
    img: path + "chefs-2.jpg",
    alt: "chefs-2",
    social: [
      {
        id: "s1",
        socialType: "facebook",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.facebook,
      },
      {
        id: "s2",
        socialType: "twitter",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.twitter,
      },
      {
        id: "s3",
        socialNamme: "instagram",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.instagram,
      },

      {
        id: "s4",
        socialName: "linkdin",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.linkdin,
      },
    ],
  },
  {
    id: "c3",
    name: "Walter White",
    position: "Master Chef",
    img: path + "chefs-3.jpg",
    alt: "chefs-3",
    social: [
      {
        id: "s1",
        socialType: "facebook",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.facebook,
      },
      {
        id: "s2",
        socialType: "twitter",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.twitter,
      },
      {
        id: "s3",
        socialNamme: "instagram",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.instagram,
      },

      {
        id: "s4",
        socialName: "linkdin",
        enable: true,
        link: "#",
        newTab: socialIconConfig.newTab,
        iconClass: socialIconConfig.linkdin,
      },
    ],
  },
];

export default { ChefContent };
