import SiteConfig from "Config/SiteConfig";
const path = SiteConfig.imagePath.eventsImage;

export const EventsContent = [
  {
    id: "e1",
    title: "event 1",
    price: "Npr. 200,000",
    description1:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    description2:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    img: path + "event-birthday.jpg",
    lists: [
      {
        id: "el1",
        paragraph: "this is list 1 of event 1",
      },
      {
        id: "el2",
        paragraph: "this is list 2 of event 1",
      },
      {
        id: "el3",
        paragraph: "this is list 3 of event 1",
      },
    ],
  },
  {
    id: "e2",
    title: "event 2",
    price: "npr 30,000",
    description1: "first description",
    description2: "second description",
    img: path + "event-private.jpg",
    lists: [
      {
        id: "el1",
        paragraph: "this is list 1 of event 2",
      },
      {
        id: "el2",
        paragraph: "this is list 2 of event 2",
      },
      {
        id: "el3",
        paragraph: "this is list 3 of event 2",
      },
    ],
  },
  {
    id: "e3",
    title: "event 3",
    price: "npr 500,000",
    description1: "first description",
    description2: "second description",
    img: path + "event-custom.jpg",
    lists: [
      {
        id: "el1",
        paragraph: "this is list 1 of event 3",
      },
      {
        id: "el2",
        paragraph: "this is list 2 of event 3",
      },
      {
        id: "el3",
        paragraph: "this is list 3 of event 3",
      },
    ],
  },
];
