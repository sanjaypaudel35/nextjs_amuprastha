import SiteConfig from "Config/SiteConfig";
const path = SiteConfig.imagePath.sliderImage;
export const CarouselContent = {
  en: [
    {
      id: "s1",
      title: "Amuprastha Boutique",
      bgImg: path + "slide-1.jpg",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
    {
      id: "s2",
      title: "Your Next Home",
      bgImg: path + "slide-2.jpg",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
    {
      id: "s3",
      title: "Share Next Event With Us",
      bgImg: path + "slide-3.jpg",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
  ],
  ne: [
    {
      id: "s1",
      title: "अमुप्रस्थ ",
      bgImg: path + "slide-1.jpg",
      description:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
    {
      id: "s2",
      title: "तपाईंको अर्को घर",
      bgImg: path + "slide-2.jpg",
      description:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
    {
      id: "s3",
      title: "तपाईको कार्यक्रमको लागि हामीलाई सम्झनुहोस्",
      bgImg: path + "slide-3.jpg",
      description:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
  ],
};
