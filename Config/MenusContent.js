export const MenusContent = {
  en: [
    {
      id: "m1",
      name: "starters",
      items: [
        {
          id: "1",
          name: "Lobster Bisque",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$5.95",
        },
        {
          id: "2",
          name: "Crab Cake",
          description:
            "A delicate crab cake served on a toasted roll with lettuce and tartar sauce",
          price: "$7.95",
        },
        {
          id: "3",
          name: "Mozzarella Stick",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$4.95",
        },
      ],
    },

    {
      id: "m2",
      name: "salad",
      items: [
        {
          id: "1",
          name: "salad 1",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$5.95",
        },
        {
          id: "2",
          name: "salad 2",
          description:
            "A delicate crab cake served on a toasted roll with lettuce and tartar sauce",
          price: "$7.95",
        },
        {
          id: "3",
          name: "salad 3",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$4.95",
        },
      ],
    },
    {
      id: "m3",
      name: "speciality",
      items: [
        {
          id: "1",
          name: "Bread barrel",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$5.95",
        },
        {
          id: "2",
          name: "Tuscan Grilled",
          description:
            "A delicate crab cake served on a toasted roll with lettuce and tartar sauce",
          price: "$7.95",
        },
        {
          id: "3",
          name: "Lobster Roll",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$4.95",
        },
      ],
    },
  ],
  ne: [
    {
      id: "m1",
      name: "starters",
      items: [
        {
          id: "1",
          name: "Lobster Bisque",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$5.95",
        },
        {
          id: "2",
          name: "Crab Cake",
          description:
            "A delicate crab cake served on a toasted roll with lettuce and tartar sauce",
          price: "$7.95",
        },
        {
          id: "3",
          name: "Mozzarella Stick",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$4.95",
        },
      ],
    },

    {
      id: "m2",
      name: "salad",
      items: [
        {
          id: "1",
          name: "salad 1",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$5.95",
        },
        {
          id: "2",
          name: "salad 2",
          description:
            "A delicate crab cake served on a toasted roll with lettuce and tartar sauce",
          price: "$7.95",
        },
        {
          id: "3",
          name: "salad 3",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$4.95",
        },
      ],
    },
    {
      id: "m3",
      name: "speciality",
      items: [
        {
          id: "1",
          name: "Bread barrel",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$5.95",
        },
        {
          id: "2",
          name: "Tuscan Grilled",
          description:
            "A delicate crab cake served on a toasted roll with lettuce and tartar sauce",
          price: "$7.95",
        },
        {
          id: "3",
          name: "Lobster Roll",
          description: "Lorem, deren, trataro, filede, nerada",
          price: "$4.95",
        },
      ],
    },
  ],
};
