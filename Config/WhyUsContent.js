export const WhyUsContent = {
  ne: [
    {
      id: "1",
      title: "गुणस्तरीय खाना",
      description:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
    {
      id: "2",
      title: "शान्ति वातावरण",
      description:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
    {
      id: "3",
      title: "व्यावसायिक शेफ",
      description:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
  ],
  en: [
    {
      id: "1",
      title: "Quality Food",
      description:
        "Lorem Ipsum मुद्रण र टाइपसेटिंग उद्योगको केवल डमी पाठ हो। Lorem Ipsum 1500s देखि नै उद्योगको मानक डमी पाठ भएको छ",
    },
    {
      id: "2",
      title: "peace Environment",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
    {
      id: "3",
      title: "Proffesional Chef",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
  ],
};
