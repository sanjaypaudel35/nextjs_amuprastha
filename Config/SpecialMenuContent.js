import SiteConfig from "Config/SiteConfig";
const path = SiteConfig.imagePath.specialMenuImage;
export const SpecialMenusItems = [
  {
    id: "sm1",
    name: "Special 1",
    title: "Special item 1",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    img: path + "specials-1.jpg",
  },
  {
    id: "sm2",
    name: "Special 2",
    title: "Special item 2",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    img: path + "specials-2.jpg",
  },
  {
    id: "sm3",
    name: "Special 3",
    title: "Special item 3",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    img: path + "specials-3.jpg",
  },
  {
    id: "sm4",
    name: "Special 4",
    title: "Special item 4",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    img: path + "specials-4.jpg",
  },
];
