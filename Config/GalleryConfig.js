import SiteConfig from "Config/SiteConfig";
const path = SiteConfig.imagePath.galleryImage;

const GalleryContent = [
  {
    id: "g1",
    name: path + "gallery-1.jpg",
    alt: "gallery 1",
    description: "this is image description 1",
  },
  {
    id: "g2",
    name: path + "gallery-2.jpg",
    alt: "gallery 2",
    description: "this is image description 2",
  },
  {
    id: "g3",
    name: path + "gallery-3.jpg",
    alt: "gallery 3",
    description: "this is image description 3",
  },
  {
    id: "g4",
    name: path + "gallery-4.jpg",
    alt: "gallery 4",
    description: "this is image description 4",
  },
  {
    id: "g5",
    name: path + "gallery-5.jpg",
    alt: "gallery 5",
    description: "this is image description 5",
  },
  {
    id: "g6",
    name: path + "gallery-6.jpg",
    alt: "gallery 6",
    description: "this is image description 6",
  },
  {
    id: "g7",
    name: path + "gallery-7.jpg",
    alt: "gallery 7",
    description: "this is image description 7",
  },
  {
    id: "g8",
    name: path + "gallery-8.jpg",
    alt: "gallery 8",
    description: "this is image description 8",
  },
];

export default {
  GalleryContent,
};
