import { useState, useEffect } from "react";
const useCheckMobileView = () => {
  let windowInnerLength;

  useEffect(() => {
    if (typeof window !== "undefined") {
      // Perform localStorage action
      windowInnerLength = window.innerWidth;
    }
  }, []);

  const [isMobile, setIsMobile] = useState(windowInnerLength < 1100);

  const DeviceIsMobile = (value) => {
    setIsMobile(value);
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener(
        "resize",
        () => {
          const ismobile = window.innerWidth < 1100;
          if (!ismobile) {
            setIsMobile(false);
          } else {
            setIsMobile(true);
          }
        },
        false
      );
    }
  }, []);

  return {
    isMobile,
    DeviceIsMobile,
  };
};
export default useCheckMobileView;
