import { useState } from "react";
const useGallery = (galleryData) => {
  const [isModalActive, setIsModalActive] = useState(false);
  const [openedImageIndex, setImageId] = useState(0);

  const showGalleryImage = (index) => {
    setIsModalActive(true);
    setImageId(index);
  };

  const modalCloseHandler = () => {
    setIsModalActive(false);
  };

  const nextImageHandler = () => {
    setImageId((prevState) => prevState + 1);
  };
  const prevImageHandler = () => {
    setImageId((prevState) => prevState - 1);
  };
  const closeModal = () => {
    modalCloseHandler();
  };

  return {
    isModalActive,
    showGalleryImage,
    modalCloseHandler,
    openedImageIndex,
    galleryData,

    nextImageHandler,
    prevImageHandler,
    closeModal,
  };
};
export default useGallery;
