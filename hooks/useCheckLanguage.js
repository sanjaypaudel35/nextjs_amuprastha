import { useContext, useEffect } from "react";
import LanguageContext from "store/language-context";

const useCheckLanguage = () => {
  // const [localStorageLang, setLocalStorageLang] = useState(null);
  const languageCtx = useContext(LanguageContext);

  useEffect(() => {
    if (typeof window !== "undefined") {
      if (localStorage.getItem("selected_lang")) {
        languageCtx.changeLang(
          localStorage.getItem("selected_lang"),
          localStorage.getItem("selected_lang_id")
        );
      }
    }
  }, []);

  return languageCtx.stateLang;
};
export default useCheckLanguage;
